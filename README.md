# Sage 200c REST API OAuth2 Token Generator PHP Example
This file repo demonstrates how to authenticate Sage 200c REST API using OAuth2. This is **NOT** to be used in a production app as it has no security checks or protections build in. It is just an example of how to use PHP in conjunction with Sage's official developer documentation found here https://developer.columbus.sage.com/docs#/uk/sage200extra/accounts.

## How to use
### Generate access token for the first time
1. You first need a Sage 200c account (do not use development credentials in a production environment). You will have to contact Sage to obtain a sandbox account if required
2. Set up a developer account and subscribe to the Sage 200c API- https://developer.columbus.sage.com/
3. Clone this repo
4. Add the correct credentials where indicated in the code found here https://developer.columbus.sage.com/docs#/uk/sage200extra/accounts/gs-authentication
5. In the **comand line** `cd` into the cloned directory and run  `$ php -S localhost:56953` to start the php dev server
6. Go to `http://localhost:56953` in a browser 
7. When prompted Sign into sage account (**see step 1**)
8. If successfull you will be prompted to allow app access. Click **'Allow'**
9. You will be redirected to a blank screen. in the root of the directory a `credentials.json` should be created with your access and refresh token in it.

### Refresh token
To refresh your access token simply follow the above steps from step 5

### Expired refresh token
If your refresh token has expired simply remove the `credentials.json` file and follow the above steps from step 5.