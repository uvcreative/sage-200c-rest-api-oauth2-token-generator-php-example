<?php
require ('account-credentials.php');

    /* for */
    header("Access-Control-Allow-Origin: *");


    function saveCredentials(  $response ){
        $credentialsFile = fopen("credentials.json", "w") or die("Unable to open file!");   
        fwrite($credentialsFile, $response);
        fclose($credentialsFile);
    }

    /* developer credentials do not use for production app - development credentials found here https://developer.columbus.sage.com/docs#/uk/sage200extra/accounts/gs-authentication*/
    $Oauth2_credentials = array(
        "clientId" => $account_credentials['clientId'], //replace with applicapble client id
        "clientSecret" => $account_credentials['clientSecret'], //replace with applicapble client Secret
        "redirectUri" =>'http://localhost:56953/Account/Authorise',
        "urlAuthorize" => 'https://signon.sso.services.sage.com/SSO/OAuthService/WebStartAuthorisationAttempt',
        "urlAccessToken" => 'https://signon.sso.services.sage.com/SSO/OAuthService/WebGetAccessToken',
        "scope" => $account_credentials['scope'], //replace with applicapble scope
        "state" => "AA_BB_CC"
    ); 
    
    $code = $Oauth2_credentials['clientId'] . ":" . $Oauth2_credentials['clientSecret'];    
  
        //check if already have creds
        $credentialFileName = "credentials.json";

        if ( file_exists($credentialFileName) ) { 
            $credentialFile= fopen( $credentialFileName , "r") or die("Unable to open file!");
            $savedCreds = JSON_decode(fread($credentialFile,filesize("credentials.json")));    
            fclose($credentialFile); 

            $curl = curl_init();
            curl_setopt_array($curl, array(
            CURLOPT_URL => $Oauth2_credentials['urlAccessToken'],
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "grant_type=refresh_token&refresh_token=". $savedCreds -> refresh_token,
            CURLOPT_HTTPHEADER => array(
                "authorization: Basic ". base64_encode( $code),
                "ContentType: application/x-www-form-urlencoded;charset=UTF-8"           
                ),
            ));
            $response = curl_exec($curl);           
            saveCredentials( $response );

        } else {
        // If we don't have an authorization code then get one
            if (!isset($_GET['code'])) { 
                //generate safe url for auth url
                $querypath= "state=".$Oauth2_credentials['state']. "&scope=". urlencode($Oauth2_credentials['scope']). "&response_type=code&redirect_uri=". urlencode($Oauth2_credentials['redirectUri']). "&client_id=". $Oauth2_credentials['clientId'];
                //redirect to Auth url
                header('Location: ' . $Oauth2_credentials['urlAuthorize'] . '?' .$querypath);
                exit;
            } 
            
            else {
                //create basic auth for getting the token
                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_URL => $Oauth2_credentials['urlAccessToken'],
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "POST",
                    CURLOPT_POSTFIELDS => "grant_type=authorization_code&code=". urlencode($_GET['code']) ."&redirect_uri=". urlencode($Oauth2_credentials['redirectUri']),
                    CURLOPT_HTTPHEADER => array(
                        "authorization: Basic ". base64_encode( $code),
                        "ContentType: application/x-www-form-urlencoded;charset=UTF-8",
                        "grant_type: authorization_code",
                    ),
                ));
                $response = curl_exec($curl);
                curl_close($curl);
                saveCredentials(  $response );
            }
        }  
   